package zd

import (
	"encoding/json"
	"fmt"
	"reflect"
	"strings"
)

// ToUpperMap
//
//	@param t
//	@return map
func ToUpperMap(t any) map[string]any {
	mp := make(map[string]any)
	tmp := toMap(t)
	for k, v := range tmp {
		mp[strings.ToUpper(k)] = v
	}
	return mp
}

// ToLittleCamelMap 小写 camel
//
//	@param t
//	@return map [string]any 结果数据(ID转id)
func ToLittleCamelMap(t any) map[string]any {
	mp := make(map[string]any)
	tmp := toMap(t)
	for k, v := range tmp {
		if k == "ID" { // id 处理
			mp["id"] = v
		} else {
			switch v := v.(type) {
			case map[string]any:
				mp[strings.ToLower(k[0:1])+k[1:]] = ToLittleCamelMap(v) // 小写 camel
			default:
				mp[strings.ToLower(k[0:1])+k[1:]] = v // 小写 camel
			}
		}
	}
	return mp
}

// toMap
//
//	@param t
//	@return map [string]any 结果数据(ID转id)
func toMap(t any) map[string]any {
	typ := reflect.TypeOf(t)
	if typ.Kind() == reflect.Ptr {
		typ = typ.Elem()
	}
	if typ.Kind() == reflect.Struct {
		return StructToMap(t, false, false)
	}
	mp := make(map[string]any)
	if typ.Kind() == reflect.Map {
		if mp, ok := t.(map[string]any); ok { // map[string][]any 不处理
			return mp
		}
		iter := reflect.ValueOf(t).MapRange()
		for iter.Next() {
			mp[iter.Key().String()] = iter.Value().Interface()
		}
		return mp
	}
	bs, _ := json.Marshal(t)
	json.Unmarshal(bs, &mp)
	return mp
}

// StructToMap 结构体转 map
//
//	@param t any 不能是指针!!!
//	@param filterZeroField bool 过滤0值字段(bool-false, int-0, string-"", nil)
//	@param filterAutoIncrementField bool 过滤自增字段(用于 insert)
//	@return map [string]any 结果数据
func StructToMap(t any, filterZeroField, filterAutoIncrementField bool) map[string]any {
	mp := make(map[string]any)
	//TypeOf会返回目标数据的类型，比如int/float/struct/指针等
	sType := reflect.TypeOf(t)
	sVal := reflect.ValueOf(t)
	if sType.Kind() == reflect.Ptr {
		// 传入的inStructPtr是指针，需要.Elem()取得指针指向的value
		sType = sType.Elem()
		sVal = sVal.Elem()
	}
	for i := 0; i < sType.NumField(); i++ {
		// 判断字段是否为结构体类型，或者是否为指向结构体的指针类型
		if sVal.Field(i).Kind() == reflect.Struct || (sVal.Field(i).Kind() == reflect.Ptr && sVal.Field(i).Elem().Kind() == reflect.Struct) { // 组合 struct
			for k, v := range StructToMap(sVal.Field(i).Interface(), filterZeroField, filterAutoIncrementField) {
				mp[k] = v
			}
			continue
		}
		typ := sType.Field(i)                          // 字段的数据类型
		if filterZeroField && sVal.Field(i).IsZero() { // 0 值不处理
			continue
		}
		val := sVal.Field(i).Interface()
		switch typ.Type.Kind() {
		case reflect.Ptr, reflect.UnsafePointer, reflect.Uintptr:
			val = sVal.Field(i).Elem()
			continue
		case reflect.Uint8: // 自定义 enum
			val = fmt.Sprintf("%c", val)
			if val == "\x00" {
				// val = "0"  // enum 未赋值,用 '0' 做初值
				continue // 未赋值不处理
			}
		}
		mp[typ.Name] = val
	}
	// 列名替换为 column 标签
	for _, v := range getColumns(t) {
		if filterAutoIncrementField && v.AutoIncrement { // 自增列由系统处理
			delete(mp, v.ColumnName)
			continue
		}
		if v.ColumnName == v.StructFieldName {
			continue
		}
		if _, ok := mp[v.StructFieldName]; ok {
			mp[v.ColumnName] = mp[v.StructFieldName] // 用数据字段名替换struct field 名称
			delete(mp, v.StructFieldName)
		}
	}
	return mp
}
