package zd

import (
	"context"

	"gitee.com/chunanyong/zorm"
)

// InitDaoOracle 初始化 oracle
//
// 应用需要 import _ "github.com/godror/godror"
//
// Go数据库驱动列表:https://github.com/golang/go/wiki/SQLDrivers
//
//	@param crmsDSN string
//	@return context.Context 数据上下文
//	@return error 错误
func InitDaoOracle(crmsDSN string) (context.Context, error) {
	//dbDaoConfig 数据库的配置.这里只是模拟,生产应该是读取配置配置文件,构造DataSourceConfig
	dbDaoConfig := zorm.DataSourceConfig{
		//DSN 数据库的连接字符串,parseTime=true会自动转换为time格式,默认查询出来的是[]byte数组
		// DSN: "root:root@tcp(127.0.0.1:3306)/readygo?charset=utf8&parseTime=true",
		DSN: crmsDSN,
		//DriverName 数据库驱动名称:mysql,postgres,oci8,sqlserver,sqlite3,go_ibm_db,clickhouse,dm,kingbase,aci,taosSql|taosRestful 和Dialect对应
		DriverName: "godror",
		//Dialect 数据库方言:mysql,postgresql,oracle,mssql,sqlite,db2,clickhouse,dm,kingbase,shentong,tdengine 和 DriverName 对应
		Dialect: "oracle",
		//MaxOpenConns 数据库最大连接数 默认50
		MaxOpenConns: 50,
		//MaxIdleConns 数据库最大空闲连接数 默认50
		MaxIdleConns: 50,
		//ConnMaxLifetimeSecond 连接存活秒时间. 默认600(10分钟)后连接被销毁重建.避免数据库主动断开连接,造成死连接.MySQL默认wait_timeout 28800秒(8小时)
		ConnMaxLifetimeSecond: 600,
		//SlowSQLMillis 慢sql的时间阈值,单位毫秒.小于0是禁用SQL语句输出;等于0是只输出SQL语句,不计算执行时间;大于0是计算SQL执行时间,并且>=SlowSQLMillis值
		SlowSQLMillis: 0,
		//DefaultTxOptions 事务隔离级别的默认配置,默认为nil
		//DefaultTxOptions: nil,
		//如果是使用分布式事务,建议使用默认配置
		//DefaultTxOptions: &sql.TxOptions{Isolation: sql.LevelDefault, ReadOnly: false},

		//FuncGlobalTransaction seata/hptx全局分布式事务的适配函数,返回IGlobalTransaction接口的实现
		//业务必须调用 ctx,_=zorm.BindContextEnableGlobalTransaction(ctx) 开启全局分布事务
		//FuncGlobalTransaction : MyFuncGlobalTransaction,

		//SQLDB 使用现有的数据库连接,优先级高于DSN
		//SQLDB : nil,

		//DisableTransaction 禁用事务,默认false,如果设置了DisableTransaction=true,Transaction方法失效,不再要求有事务,为了处理某些数据库不支持事务,比如TDengine
		//禁用事务应该有驱动伪造事务API,不应该有orm实现,clickhouse的驱动就是这样做的
		// DisableTransaction: true, // 禁用全局事务
	}

	// 根据dbDaoConfig创建dbDao, 一个数据库只执行一次,第一个执行的数据库为 defaultDao,后续zorm.xxx方法,默认使用的就是defaultDao
	dbDao, err := zorm.NewDBDao(&dbDaoConfig)
	if err != nil {
		return nil, err
	}

	// ctx默认应该有 web层传入,例如gin的c.Request.Context().这里只是模拟
	ctx := context.Background()
	return dbDao.BindContextDBConnection(ctx)
}
