package zd

import (
	"fmt"
	"testing"
)

func TestStructTomap(t *testing.T) {
	// Gender: "", IsActive: false, 条件无效
	u := UserInfo{Name: "abc", Gender: "", IsActive: false, Status: STst, ID: 11}
	fmt.Println(StructToMap(u, true, false))
	fmt.Println(StructToMap(u, true, true))
	fmt.Println(StructToMap(u, false, false))
	fmt.Println(StructToMap(u, false, true))
}
