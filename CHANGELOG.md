### v1.1.8

- 修复: SeleceCount bug(Oracle)
- 新增：DisableTransaction 关闭事务

### v1.1.7

- 新增: SelectMapCamel 返回 key 小驼峰格式(SelectMap 改为全大写 key)

### v1.1.6

- 修复: 重复建表时是否存在的判断 bug

### v1.1.5

- 修复: Update Struct 自定义 float64 被转为%c 的 bug
- 修复: mapToFinder 特殊 key 的 bug
- 修复: SelectRowByName 返回值的 bug

### v1.1.4

- 修复: 查询条件为 > n 时的 bug(增加基础类型处理)
- 更新: SelectMapColumns 返回 map 改为全写格式(camel 格式在遇到 count(1)时无法处理)

### v1.1.3

- 更新: SelectMap SelectMapColumns 返回的 map 的 key 采用 camel 格式,以方便前端显示
- 删除: gin 相关函数, 独立为 gitee.com/haifengat/gin-ex 项目

### v1.1.2

- 新增: Select 自定义类型的数组([]InstrumentIDType)用 in 查询
- 新增: ToLittleCamelMap 处理第 2 层的 Key(map[string]map[string]any)
- 新增: ByName 查询只传入表名时,加上默认 schemaName
- 优化: gin.Context 命名 c -> ctx
- 修复: CreateHandler 时出现的 wrote more than the declared Content-Length 错误
- 删除: ResponseOk ResponseErr, 需用 CreateHandler 替代
- 修复: execSql 不使用事务时报错
- 更新: getObjects 支持 query,json 同时生效
- 更新: 根据 CreateHandler, 修改 gin 相关的函数返回值
- 优化: CreateHandler 返回 data,data2,error
- 新增: CreateHandler 测试用例
- 修复: ResponseErr errors.UnWrap 遇到非 wrap 的 err 报错
- 更新: utils 工具改为 public 访问
- 新增: CreateHandler gin 装饰器

### v1.1.1

- 更新: 函数说明
- 升级: zorm v1.6.9
- 更新: gin 函数说明

### v1.0.22

- 修复: getSQL 时防注入的错误(前面加 fnd.InjectionCheck = false)
- 修复: update/insert 时 nil 值不能被过滤(update 时尤其如此)
- 修复: Query 空值 nil 的 bug, 使用 IsNull(xxx) = true
- 优化: 提示信息用 /\*\*/ 以支持换行显示
- 修复: update/insert 时 nil 值不能被过滤(update 时尤其如此)

### v1.0.21

- 更新: zorm v1.6.7
- 修复: 使用占位符防止注入的错误提示
- 修复: SelectMapColumns columns 未传递的 bug

### v1.0.20

- 修复: Update[T] 自增作为主键的处理 bug

### v1.0.19

- 新增：GetQryParams 支持 append 指定添加 sql（如 order by, group by 等）
- 修复：mapToFinder 函数中 params 类型为 map[string]map[string]any 时未对自定义类型转换，引起的 bug
- 修复：无自增字段的表执行 SET IDENTITY_INSERT ON 报错

### v1.0.18

- 新增: insert 时开启 SET IDENTITY_INSERT $tblName ON, 以适应自增字段
- 新增: 自定义 BoolType
- 新增: ExecuteSql 执行 sql 语句
- 修复: Select[T] 函数未返回 page 中的 total
- 修复: RegNumberType 后自定义 float64 的转换
- 新增: RegNumberType 注册 dm/oracle 的 number/numeric 类型转 float64
- 新增: RegEnumByteType 注册 byte 自定义的 enum 类型, 转为 string

### v1.0.17

- 修复: Update 更新数据为 0 时报错, 会影响前端处理逻辑
- 修复: Select[T] 函数处理自定义 byte 类型的 bug

### v1.0.16

- 更新: enumtype 适应 v1.6.4 更新
- 更新: zorm v1.6.4
- 新增: 过滤自增字段,用于 insert 操作.
- 新增: SetDefaultCtx 与 InitDaoOracle 配合, 实现对 oracle 库操作.
- 新增: InitDaoOracle 初始化 oracle
- 新增: 支持 column 定义列名后的 select/insert/update/delete
- 新增: InsertNotZero 插入数据时过滤 0 值
- 新增: SelectByStruct 用 T 作为查询条件
- 新增: structToMap 增加 notZero 参数
- 新增: QueryMap 复杂 sql 语句查询

### v1.0.15

- 删除: Entity 的 InsertTime, UpdateTime, 由应用处理
- 新增: GetTableName[T] 函数中,读取 t.GetTableName()

### v1.0.14

- 修复: QueryMap 时 Numeric 转 Json 为 string 的问题
- 新增: mapToFinder 数组中增加 []any
- 修复: mapToFinder 传入 nil 时导致的错误
- 增加: default bool 型支持 1,t,T,true,TRUE,True
- 修复: GetQryParams p 初始化后 PageSize 为 0 导致查询时,返回除数为 0 的错误.
- 修复: PageSize 为 0 时出现除数为 0 的错误

### v1.0.13

- 新增: InitDao 增加 error 返回
- 升级: dm 驱动 v.1.8.9, 解决时间格式问题
- 修复: GetPrimarykeyByName 更名 GetPrimaryKeyByName
- 修复: ToMap 判断类型为 struct/map/others

### v1.0.12

- 优化: 函数名标准: 默认-Struct, Map-使用 Map, ByName-有表名参数
- 修复: 多字段名查询的 bug
- 新增: Insert/InsertMap 支持批量插入
- 新增: Update 支持 struct 更新
- 新增: 增删改操作默认开启事务
- 更新: 完善 sql_test 用例

### v1.0.11

- 修复: mapToFinder 自定义类型处理 bug (支持 byte/int/float64/string/bool)
- 修复: SelectCount 中 tblName 参数多余, 删除

### v1.0.9

- 更新: go.mod 1.19
- 新增: SelectCountByName, SelectRow, SelectRowByName, SelectMapColumns, SelectMapColumnsByName
- 新增: InsertSlice, InsertStruct
- 更新: DDL 开启事务
- 更新: ioutil 用 io 替换 (1.6 弃用)
- 优化: mapToFinder

### v1.0.8

- 新增: 事务返回 error, 以方便应用层判断后处理后续

### v1.0.7

- 修复: EnumType 支持 struct 查询

### v1.0.6

- 新增: 封装响应函数 ResponseOk ResponseErr
- 更新: 修改包名由 zorm_dm 改为 zorm-dm

### v1.0.5

- 新增: 查询时支持 between .. and ..和运算符(>,<,>=,<=)

### v1.0.4

- 修复: 分页查询未生效, 因 GetQryParams 函数中的 p 未初始化

### v1.0.2

- 新增: 开启事务 Transaction
- 修复: SelectCount bug

### v1.0.1

- 新增: SelectCount
- 优化: GetObject 代码
- 修复: queryMapByName 时,ConverDiverValue 报错(structFieldType 为 nil)

### v1.0.0

- zorm: v1.6.3
- 数据库: create/drop
- 数据表: select/insert/update/delete
- 自定义类型: byte/rune/string
- gin: Get/Post/Put/Delete
