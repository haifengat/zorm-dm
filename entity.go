package zd

import (
	"bytes"
	"context"
	"fmt"
	"html/template"
	"reflect"
	"strconv"
	"strings"

	"gitee.com/chunanyong/zorm"
	"github.com/pkg/errors"
)

var SchemaName = "SYSDBA" // 模式

type IEntity interface {
	zorm.IEntityStruct
	GetPrimaryKey() []string // 主键
}

// Entity 实现 IEntity
type Entity struct{}

// GetTableName 返回struct类型名;需要struct自行实现
func (Entity) GetTableName() string { return "" }

// 用 GetPrimaryKey[T] 替代
func (Entity) GetPKColumnName() string { return "" }

func (Entity) GetPkSequence() string { return "" }

// GetPrimaryKey 表的主键, 返回 nil 时忽略此函数; 否则以此函数为准, 忽略 tag中的 primaryKey 配置
func (Entity) GetPrimaryKey() []string { return nil }

// GetDefaultValue 获取列的默认值Map,用于Insert和Update Struct对象,UpdateNotZeroValue请使用BindContextMustUpdate方法.返回map的key是Struct属性名,value是默认值,value可以是nil.
func (Entity) GetDefaultValue() map[string]interface{} { return nil }

// GetTableName 取表名
//
//	@return string SchemaName.TblName(大写)
func GetTableName[T IEntity]() string {
	tblName := (*new(T)).GetTableName()
	if tblName != "" {
		if !strings.Contains(tblName, ".") {
			tblName = SchemaName + "." + tblName
		}
	} else {
		tblName = strings.ToUpper(reflect.TypeOf((*T)(nil)).Elem().Name()) // 表名大写
		if len(SchemaName) > 0 {
			tblName = SchemaName + "." + tblName
		}
	}
	return tblName
}

// TableColumn 数据列定义
type TableColumn struct {
	ColumnName      string // 数据表字段名(字段名or由columns定义)
	StructFieldName string // 结构体字段名称
	ColumnType      string // 数据表中的类型定义
	Comment         string
	PrimaryKey      bool
	NotNull         bool
	Unique          bool
	Index           bool
	AutoIncrement   bool // 自增
	Default         string
}

// GetColumnName 取列名(从dm库中取)
//
//	@param tblName 表名,格式: SchemaName.TblName
//	@return []string 列名表
func GetColumnName(tblName string) []string {
	var keys = make([]string, 0)
	err := zorm.Query(ctxDefault, zorm.NewFinder().Append("SELECT COLUMN_NAME FROM all_tab_columns WHERE owner = ? AND table_name = ?", strings.Split(tblName, ".")[0], strings.Split(tblName, ".")[1]), &keys, nil)
	if err != nil {
		fmt.Print(err)
	}
	return keys
}

// GetPrimaryKey 取表的主键
//
//	@return []string 主键(全大写)
func GetPrimaryKey[T IEntity]() []string {
	t := *new(T)
	// 用表的函数实现覆盖 tag
	if keys := t.GetPrimaryKey(); keys != nil {
		return keys
	}
	keys := make([]string, 0)
	for _, v := range GetColumns[T]() {
		if v.PrimaryKey {
			keys = append(keys, v.ColumnName)
		}
	}
	return keys
}

// GetColumns 取列定义
//
//	@return []TableColumn 数据列定义表(列名大写)
func GetColumns[T IEntity]() []TableColumn {
	cols := getColumns(*new(T))
	for i := range cols {
		c := &cols[i]
		c.ColumnName = strings.ToUpper(c.ColumnName) // 列名大写
	}
	return cols
}

func getColumns(t any) []TableColumn {
	//TypeOf会返回目标数据的类型，比如int/float/struct/指针等
	sType := reflect.TypeOf(t)
	sVal := reflect.ValueOf(t)
	if sType.Kind() == reflect.Ptr {
		// 传入的inStructPtr是指针，需要.Elem()取得指针指向的value
		sType = sType.Elem()
		sVal = sVal.Elem()
	}
	// struct 字段组
	columns := make([]TableColumn, 0)
	for i := 0; i < sType.NumField(); i++ {
		// 判断字段是否为结构体类型，或者是否为指向结构体的指针类型
		if sVal.Field(i).Kind() == reflect.Struct { // 组合 struct
			cols := getColumns(sVal.Field(i).Interface())
			columns = append(columns, cols...) // 子 struct
			continue
		}
		typ := sType.Field(i) // 字段的数据类型
		column := TableColumn{
			ColumnName: typ.Name,
		}
		// 处理标签约定
		tag := typ.Tag
		tags := make(map[string]string) // 标签名均转换为小写
		if tagStr := tag.Get("zorm"); tagStr != "" {
			for _, v := range strings.Split(tagStr, ";") {
				kv := strings.Split(v, ":")
				if len(kv) == 2 {
					tags[strings.ToLower(kv[0])] = kv[1]
				} else {
					tags[strings.ToLower(kv[0])] = ""
				}
			}
		}
		switch typ.Type.Kind() {
		case reflect.Int:
			column.ColumnType = "int"
		case reflect.String:
			if size, ok := tags["size"]; ok {
				column.ColumnType = "VARCHAR(" + size + ")"
			} else {
				column.ColumnType = "VARCHAR(256)"
			}
		case reflect.Float64:
			column.ColumnType = "NUMBER(22, 6)"
		case reflect.Bool:
			column.ColumnType = "BIT"
		case reflect.Ptr, reflect.UnsafePointer, reflect.Uintptr:
			continue
		case reflect.Uint8: // 自定义 rune
			if typ.Type.Name() != "uint8" {
				column.ColumnType = "char(1)"
			} else {
				column.ColumnType = "int"
			}
		case reflect.Int32: // 自定义 rune
			if typ.Type.Name() != "int32" {
				column.ColumnType = "char(1)"
			} else {
				column.ColumnType = "int"
			}
		default:
			fmt.Print("不支持的类型:", column.ColumnName, typ.Type.Align(), typ.Type.Kind())
			continue
		}

		// ColumnName
		if columnName, ok := tags["column"]; ok {
			column.ColumnName = columnName
		}
		// ColumnType
		if typeName, ok := tags["type"]; ok {
			column.ColumnType = typeName
		}
		// GetDataType 函数自定义类型
		v := sVal.Field(i)
		if m := v.MethodByName("GetDataType"); m.IsValid() { // 零值的IsValid方法返回false，其Kind方法返回Invalid，其String方法返回""，对零值的任何其他方法的调用均会导致panic
			if dateType := m.Call(nil); len(dateType) > 0 {
				column.ColumnType = dateType[0].String()
			}
		}
		if def, ok := tags["default"]; ok {
			if b, err := strconv.ParseBool(def); err == nil && b {
				column.Default = "1"
			} else {
				column.Default = "0"
			}
		}
		_, column.PrimaryKey = tags["primarykey"]
		_, column.AutoIncrement = tags["autoincrement"] // 所有 tag 全小写
		_, column.Unique = tags["unique"]
		_, column.Index = tags["index"]
		_, column.NotNull = tags["not null"]
		column.Comment = tags["comment"]
		column.StructFieldName = typ.Name
		columns = append(columns, column)
	}
	return columns
}

// CreateSchema creates a new schema
//
//	@param schemaName
//	@return error
func CreateSchema(schemaName string) error {
	return execSql(fmt.Sprintf(`CREATE SCHEMA %s AUTHORIZATION SYSDBA;`, schemaName))
}

// DropTable drops a table
//
//	@return error
func DropTable[T IEntity]() error {
	// defer dbDao.CloseDB()
	return execSql(fmt.Sprintf(`drop table if exists %s`, GetTableName[T]()))
}

// TrunTable truncates a table
//
//	@return error
func TrunTable[T IEntity]() error {
	// defer dbDao.CloseDB()
	return execSql(fmt.Sprintf(`truncate table %s`, GetTableName[T]()))
}

// CreateTable creates a new table
//
// 主键primaryKey;列名column:User_Name;长度size:12;非空not null;索引index;默认default
//
//	@return error
func CreateTable[T IEntity]() error {
	// defer dbDao.CloseDB()
	tblName := GetTableName[T]()
	mps, err := QueryMap(fmt.Sprintf("SELECT COUNT(1) FROM dba_segments WHERE owner='%s' AND segment_name='%s'", strings.Split(tblName, ".")[0], strings.Split(tblName, ".")[1]))
	if err != nil {
		return err
	}
	for _, v := range mps[0] {
		if v.(int64) > 0 {
			return nil
		}
	}
	tmplStr := fmt.Sprintf(`CREATE TABLE  if not exists %s
	(
	{{range $i,$v := .}}{{if gt $i 0}}, {{end}}{{.ColumnName}} {{.ColumnType}}
	{{end}}) STORAGE(ON "MAIN", CLUSTERBTR) ;`, tblName)

	tmplCreate, _ := template.New("create").Parse(tmplStr)
	columns := GetColumns[T]()
	var buf = bytes.Buffer{}
	tmplCreate.Execute(&buf, columns)

	// create table 用 ?.? 报错, 可能与 string 加 ' 有关
	err = execSql(buf.String())
	if err != nil {
		return errors.Wrap(err, "create table")
	}

	primarys := make([]string, 0)
	for _, v := range columns {
		// not null 非空
		if v.NotNull {
			err = execSql(fmt.Sprintf(`alter table %s alter column %s set not null;`, tblName, v.ColumnName))
			if err != nil {
				return errors.Wrap(err, "alter not null")
			}
		}
		if v.PrimaryKey {
			primarys = append(primarys, v.ColumnName)
		}
		// default 默认值
		if v.Default != "" {
			err = execSql(fmt.Sprintf(`alter table %s alter column %s set default (%s);`, tblName, v.ColumnName, v.Default))
			if err != nil {
				return errors.Wrap(err, "alter default")
			}
		}
		// 自增
		if v.AutoIncrement {
			err = execSql(fmt.Sprintf(`alter table %s add column %s identity(1, 1);`, tblName, v.ColumnName))
			if err != nil {
				return errors.Wrap(err, "alter default")
			}
		}
		// unique 唯一索引
		if v.Unique {
			err = execSql(fmt.Sprintf(`create unique index if not exists %s_%s on %s(%s);`, tblName, v.ColumnName, tblName, v.ColumnName))
			if err != nil {
				return errors.Wrap(err, "alter default")
			}
		} else { // index 索引
			if v.Index {
				err = execSql(fmt.Sprintf(`create index if not exists %s_%s on %s(%s);`, tblName, v.ColumnName, tblName, v.ColumnName))
				if err != nil {
					return errors.Wrap(err, "alter default")
				}
			}
		}
		// comment 注释
		if v.Comment != "" {
			err = execSql(fmt.Sprintf(`comment on column %s.%s is '%s';`, tblName, v.ColumnName, v.Comment))
			if err != nil {
				return errors.Wrap(err, "alter default")
			}
		}
		// check 阀值检查(例 direction 只能是 0买 1卖, 根据声明对应的 enum 自动生成)
	}
	// primaryKey 主键; 可用函数实现覆盖
	if keys := GetPrimaryKey[T](); keys != nil {
		primarys = keys
	}
	if len(primarys) > 0 {
		err = execSql(fmt.Sprintf(`alter table %s add primary key(%s);`, tblName, strings.Join(primarys, ",")))
		if err != nil {
			return errors.Wrap(err, "alter primarykey")
		}
	}
	return nil
}

func execSql(sql string, values ...any) error {
	_, err := zorm.Transaction(ctxDefault, func(ctx context.Context) (interface{}, error) {
		var finder = zorm.NewFinder()
		finder.InjectionCheck = false
		finder.Append(sql, values...)
		finder.InjectionCheck = false
		finder.GetSQL()
		return zorm.UpdateFinder(ctx, finder)
	})
	if err != nil {
		return errors.Wrap(err, "执行 SQL")
	}
	return nil

}
