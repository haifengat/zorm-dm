package zd

import (
	"context"
	"database/sql"
	"database/sql/driver"
	"reflect"
	"unsafe"

	"gitee.com/chunanyong/zorm"
)

// RegBoolType 注册自定义 bool 类型
func RegBoolType() {
	zorm.RegisterCustomDriverValueConver("dm.BIT", BoolType{})
}

type BoolType struct{}

// GetDriverValue 根据数据库列类型,返回driver.Value的实例,struct属性类型
// 非struct类型接收,无法获取到structFieldType,会传入nil
func (e BoolType) GetDriverValue(ctx context.Context, columnType *sql.ColumnType, structFieldType *reflect.Type) (driver.Value, error) {
	return new(bool), nil
}

// ConverDriverValue 数据库列类型,GetDriverValue返回的driver.Value的临时接收值,struct属性类型
// 非struct类型接收,无法获取到structFieldType,会传入nil
// 返回符合接收类型值的指针,指针,指针!!!!
func (e BoolType) ConverDriverValue(ctx context.Context, columnType *sql.ColumnType, tempDriverValue driver.Value, structFieldType *reflect.Type) (interface{}, error) {
	val := tempDriverValue.(*bool)
	if structFieldType != nil { // map 方式时为 nil(因无法取到 structField)
		// 变量类型为 structFieldType 否则提示类型不匹配
		v := reflect.NewAt(*structFieldType, unsafe.Pointer(val))
		return v.Interface(), nil
	}
	return val, nil
}
