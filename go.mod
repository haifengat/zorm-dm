module gitee.com/haifengat/zorm-dm

go 1.19

require (
	gitee.com/chunanyong/dm v1.8.12
	gitee.com/chunanyong/zorm v1.7.4
	github.com/pkg/errors v0.9.1
)

require (
	github.com/golang/snappy v0.0.4 // indirect
	golang.org/x/text v0.9.0 // indirect
)
