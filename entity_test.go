package zd

import (
	"fmt"
	"testing"
)

type UserNameType string

func (UserNameType) GetDataType() string {
	return "varchar(14)"
}

// char(1) 与自定义类型对应
type StatusType byte

const STst StatusType = '0'
const STnd StatusType = '1'

type Depart struct {
	ID    string
	Name  string
	Count int
}

type UserInfo struct {
	Entity                // 实现 GetTableName (返回struct名称,不带schema)
	Name     UserNameType `zorm:"primarykey;column:User_Name;size:12;not null;index;unique"`
	Gender   string       `zorm:"primarykey;index;comment:性别"`
	Age      int
	IsActive bool
	Profit   float64
	Status   StatusType
	ID       int    `zorm:"not null;autoIncrement"`
	Remark   string `zorm:"size:32"`
}

type Employee struct {
	UserInfo
	Name     string `zorm:"comment:333XX"` // 尝试覆盖,令userInfo.Name 配置失效
	EmpleeID string `zorm:"primaryKey"`
}

func (Employee) GetPrimaryKey() []string {
	return []string{"Name"}
}

func TestCreateSchema(t *testing.T) {
	t.Run("创建模板", func(t *testing.T) {
		err := CreateSchema(SchemaName)
		fmt.Println(err)
	})
}

func TestCreateUserInfo(t *testing.T) {
	err := DropTable[UserInfo]()
	fmt.Println(err)
	err = CreateTable[UserInfo]()
	fmt.Println(err)
}

// TestEntityCompenent 测试组合 struct
func TestEntityEmployee(t *testing.T) {
	err := DropTable[Employee]()
	if err != nil {
		t.Error(err)
	}
	err = CreateTable[Employee]()
	if err != nil {
		t.Error(err)
	}
}
