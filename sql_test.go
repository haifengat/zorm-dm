package zd

import (
	"errors"
	"fmt"
	"testing"
)

func TestSelectMapColumns(t *testing.T) {
	fmt.Println(SelectMapByName(GetTableName[UserInfo](), nil, map[string]any{"User_Name": "abc11"}, "Order by User_Name", "Limit 1"))
}

func TestUpdateStruct(t *testing.T) {
	cases := []struct {
		Name     string
		Entities []UserInfo
	}{
		{"bool , Status 不赋值", []UserInfo{{Name: "d11", Gender: "f", Age: 7}}},
		{"缺少主键 Gender", []UserInfo{{Name: "d1", IsActive: true}}},
		{"多条时是否触发事务", []UserInfo{
			{Name: "abc11", Gender: "male", Age: 100},
			{Name: "d10", Gender: "f", Age: 8},
		}},
	}
	for _, c := range cases {
		t.Run(c.Name, func(t *testing.T) {
			n, err := Update(c.Entities...)
			fmt.Println(c.Name, n, err)
		})
	}
}
func TestUpdateMap(t *testing.T) {
	where := make(map[string]any)
	where["user_name"] = "abc11"

	set := make(map[string]any)
	set["age"] = 18 // Summer 都是 int 可转换
	set["isactive"] = true
	set["profit"] = 20.03
	set["gender"] = "male"
	set["status"] = STnd
	set["Remrk"] = nil // NULL
	cases := []struct {
		Name string
		Set  map[string]any
	}{
		{"正常数据", set},
		{"Status 字符", map[string]any{"status": '0'}},
		{"Status 字符串", map[string]any{"status": "1"}},
		{"NULL 赋值", map[string]any{"Remark": nil}},
	}
	for _, c := range cases {
		t.Run(c.Name, func(t *testing.T) {
			n, err := UpdateMapConditionsByName(GetTableName[UserInfo](), []map[string]any{where}, c.Set)
			fmt.Println(c.Name, n, err)
		})
	}
}

func TestSelectByStruct(t *testing.T) {
	rows, err := SelectByStruct(nil, UserInfo{Name: "abc11"}) // " order by IsActive")
	fmt.Println("查询(用struct作为条件)", rows, err)
}

func TestSelect(t *testing.T) {
	cases := []struct {
		Name  string
		Where map[string]any
	}{
		// {"查询所有", nil},
		// {"数组用 in", map[string]any{"user_name": []string{"abc11", "f"}}},
		// {"status enum", map[string]any{"status": STst}},
		// {"status 字符", map[string]any{"status": '0'}},
		{"NULL", map[string]any{"Remark": nil}},
	}
	for _, c := range cases {
		rows, err := Select[UserInfo](nil, c.Where)
		fmt.Println(c.Name, rows, err)
	}
}

func TestSelectMap(t *testing.T) {
	cases := []struct {
		Name    string
		Where   map[string]any
		Columns []string
		AppStr  []string
	}{
		{"查询所有", nil, nil, nil},
		{"数组用 in", map[string]any{"user_name": []string{"d", "f"}}, nil, nil},
		{"范围 between", map[string]any{"age": map[string]any{"between": []any{7, 20}}}, nil, nil},
		{"符号 >=", map[string]any{"age": map[string]any{">=": 10}}, nil, nil},
		{"status enum", map[string]any{"status": STst}, nil, nil},
		{"status 字符", map[string]any{"status": '0'}, nil, nil},
		{"指定字段", map[string]any{"status": '0'}, []string{"user_name", "age"}, nil},
		{"指定字段", map[string]any{"status": '0'}, []string{"max(age)"}, nil},
		{"group", nil, []string{"status", "max(age)"}, []string{"group by status"}},
	}
	for _, c := range cases {
		rows, err := SelectMapColumnsByName(GetTableName[UserInfo](), nil, c.Where, c.Columns, c.AppStr...)
		fmt.Println(c.Name, rows, err)
	}
}

func TestInsertNotZero(t *testing.T) {
	err := InsertNotZero(UserInfo{Name: "notZero", Gender: "f"})
	fmt.Println(err)
}

func TestInsert(t *testing.T) {
	cases := []UserInfo{
		{Name: "abc12", Gender: "m", IsActive: false, Status: STst, Age: 22},
		{Name: "d1", Gender: "f", IsActive: true, Status: STnd, Age: 33},
		{Name: "abc123", Gender: "m", IsActive: false, Status: STst, Profit: 234.33},
		{Name: "d10", Gender: "f", IsActive: true, Status: STnd, Profit: 234.22},
	}

	err := Insert(cases...)
	fmt.Println(err)
}

func TestInsertMap(t *testing.T) {
	cases := []map[string]any{
		// {"User_Name": "abc11", "Gender": "m", "Status": STst},
		// {"User_Name": "d11", "Gender": "f", "IsActive": true, "Status": "1"},
		{"User_Name": "abc112", "Gender": "m", "Status": STst, "Remark": nil},
	}
	for _, c := range cases {
		err := InsertMap[UserInfo](c)
		fmt.Println(err)
	}
}

func TestDelete(t *testing.T) {
	cases := []map[string]any{
		{"User_Name": "abc12"},
		{"Status": STnd},
	}
	for _, c := range cases {
		n, err := Delete[UserInfo](c)
		fmt.Println(n, err)
	}
	// 事务
	Transaction(func() error {
		n, err := Delete[UserInfo](map[string]any{"Status": STst})
		fmt.Println(n, err)
		return errors.New("回滚")
	})
}

func TestQueryMap(t *testing.T) {
	rows, err := QueryMap(`SELECT COUNT(DISTINCT gender) FROM FEEMGR.USERINFO where GENDER = ?
	union 
	SELECT SUM(age)  FROM FEEMGR.USERINFO`, "m")
	fmt.Println(rows, err)
}

func init() {
	InitDao("dm://SYSDBA:SYSDBA001@localhost:5236")
	SchemaName = "SYSDBA" // 测试默认模式
}
func Test(t *testing.T) {
	TestCreateUserInfo(t) // 新建表
	fmt.Println("TestInsertMap")
	TestInsertMap(t)
	fmt.Println("TestInsert")
	TestInsert(t) // 需处理自增列

	// 检查 byte 自定义类型的值
	users, err := Select[UserInfo](nil, nil)
	fmt.Println(users, err)

	fmt.Println("TestQueryMap")
	TestQueryMap(t) // 执行特定 sql 语句
	fmt.Println("TestSelectByStruct")
	TestSelectByStruct(t) // 测试指定 column 列名是否正确转换
	fmt.Println("TestSelectMap")
	TestSelectMap(t)
	fmt.Println("TestSelectMapColumns")
	TestSelectMapColumns(t)
	fmt.Println("TestUpdateMap")
	TestUpdateMap(t)
	fmt.Println("TestUpdateStruct")
	TestUpdateStruct(t) // 试图修改自增列[ID]
	fmt.Println("TestDelete")
	TestDelete(t)
}
