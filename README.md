# zorm-md

## 介绍

后端项目封装。将应用与数据库、前端的逻辑进行封装，在此封装的基础上进行后端项目开发，只需关注在业务逻辑。

## 软件架构

以 struct 为基础，实现数据库的各类操作封装(Create/Drop/Truncate/Select/Insert/Update/Delete)，为 apiFox 提供数据结构所需的 json 代码(camel 格式)，生成或更新为前端 vue 所用的 interface。

## 使用说明

### 安装达梦(docker)

1. 下载镜像 https://eco.dameng.com/download/
2. 安装
   ```sh
   docker load -i dm8_20220822_rev166351_x86_rh6_64_ctm.tar
   docker run -d -p 5236:5236 --restart=always --name dm8 --privileged=true -e PAGE_SIZE=16 -e LD_LIBRARY_PATH=/opt/dmdbms/bin -e INSTANCE_NAME=dm8_01 -v /data/dm8_01:/opt/dmdbms/data dm8_single:v8.1.2.128_ent_x86_64_ctm_pack4
   ```

### 数据库操作使用 zorm 框架

- 初始化 InitDao(dmDSN)

- 数据表函数(create/drop/truncate)统一为 func\[type]()

- GetTableName 默认取 struct 类别名作为表名(无 schema), 如需改名或增加 schema 则需实现 GetTableName 自定义函数.
- GetDataType 自定义类型
  - 以 string 的别名, 实现 GetDataType()string 返回 "varchar(nn)", 以实现特定类型的定义, 并在多处重复使用.

### 工具

- [x] ToUpperMap 类型转 map 并 key 全大写, 以适应数据库字段匹配
- [x] ToLittleCamelMap 类型转 map key 采用 camel 格式
- [x] StructToJson 转为 json 为 apiFox 使用

### 数据库

- [x] Create Table
  - [x] GetTableName
  - [x] GetPrimaryKey
  - [x] GetColumns
  - [x] ~~struct 组合~~
    - [x] ~~重名字段的定义以父 struct 为准~~
    - [x] ~~zorm.Select 遇到重名字段会报错~~
  - [x] tag: primaryKey/not null/size/default/index/comment
  - [ ] tag: unique/check
  - [x] 自定义类型
    - [x] type xxx string
    - [x] type xxx byte
    - [x] DM 读取 char(1)转为 byte 的自定义类型
    - [x] type xxx bool
    - [x] DM 读取 BIT 转为 bool 自定义类型
    - [x] type xxx float64
    - [x] DM 读取 number 转为 float64 自定义类型
  - [x] 基础类型: int float64 bool
- [x] Drop Table
- [x] Truncate Table
- [x] select/find
- [x] insert
- [x] update
- [x] 开启事务

### 自定义类型转换

```go
/*
自定义 ENUM 类型 type XXX byte
const AAA XXX = '0'
*/
package zd

import (
	"context"
	"database/sql"
	"database/sql/driver"
	"reflect"
	"unsafe"

	"gitee.com/chunanyong/zorm"
)

// RegEnumByteType 自定义 byte 的 enum
func RegEnumByteType() {
	//RegisterCustomDriverValueConver 注册自定义的字段处理逻辑,用于驱动无法直接转换的场景,例如达梦的 TEXT 无法直接转化成 string
	//一般是放到init方法里进行注册
	zorm.RegisterCustomDriverValueConver("dm.CHAR", EnumType{})
}

type EnumType struct{}

// GetDriverValue 根据数据库列类型,返回driver.Value的实例,struct属性类型
// 非struct类型接收,无法获取到structFieldType,会传入nil
func (e EnumType) GetDriverValue(ctx context.Context, columnType *sql.ColumnType, structFieldType *reflect.Type) (driver.Value, error) {
	return new(string), nil
}

// ConverDriverValue 数据库列类型,GetDriverValue返回的driver.Value的临时接收值,struct属性类型
// 非struct类型接收,无法获取到structFieldType,会传入nil
// 返回符合接收类型值的指针,指针,指针!!!!
func (e EnumType) ConverDriverValue(ctx context.Context, columnType *sql.ColumnType, tempDriverValue driver.Value, structFieldType *reflect.Type) (interface{}, error) {
	val := tempDriverValue.(*string)
	if len(*val) == 1 {
		if structFieldType == nil { // map 方式时为 nil(因无法取到 structField)
			return val, nil // zorm.QueryMap 自定义 byte 取值为 "1"
		}
		if kind := (*structFieldType).Kind(); kind == reflect.Uint8 && (*structFieldType).Name() != "uint8" {
			// 变量类型为 structFieldType 否则提示类型不匹配
			c := byte((*val)[0])
			v := reflect.NewAt(*structFieldType, unsafe.Pointer(&c))
			return v.Interface(), nil
		}
	}
	return val, nil
}
```

### 示例

[sql 增删改查](sql_test.go)
[table 建删清空](entity_test.go)
